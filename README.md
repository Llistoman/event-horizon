# EVENT HORIZON

[<img src="imgs/2.png">](https://llistoman.itch.io/event-horizon)

## Synopsis

***Event Horizon is a 2D space shooter made with Unity's latest version (2019.2.17f1) and using free assets as part of an assignment for a job application.***

You've been trapped inside a mysterious nebula and you can't seem to get out of it. Your enemies have found you, but you won't surrender. Black hole bombs are at your disposal, but beware of the event horizon. You won't come back alive from it...

To download, check the [itch.io page](https://llistoman.itch.io/event-horizon) or this repository releases.

## Controls

- WASD -> Move Ship
- SPACEBAR -> Brake
- Left Click -> Fire missile
- Right Click -> Leave Black Hole Bomb

## Assets used

- [Space Shooter GUI](https://craftpix.net/freebies/free-space-shooter-game-gui/)
- [Dynamic Space Background](https://assetstore.unity.com/packages/2d/textures-materials/dynamic-space-background-lite-104606)
- [2D Space Kit](https://assetstore.unity.com/packages/2d/environments/2d-space-kit-27662)
- [Music from](https://www.dl-sounds.com/royalty-free/sci-fi-pulse-loop/)

And more miscelaneous from other sites, like the [explosion](https://opengameart.org/content/pixel-explosion-12-frames)

<p align="center">
	<img src="imgs/gameplay2.gif">
	<img src="imgs/laser.gif">
	<img src="imgs/black-hole.gif">
	<img src="imgs/gameplay.gif">
	<img width=805 height=500 src="imgs/main.png">
</p>
