﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : ShipBase
{
    // References
    private WaitForSeconds textDelay;
    private WaitForSeconds missileDelay;
    private WaitForSeconds blackHoleDelay;
    private bool brakes = false;
    private bool canFire = true;
    private bool canBlackHole = true;

    [Header("Missile Parameters")]
    public GameObject missile_prefab;
    public float mInstance_dist = 1.5f;
    public float missile_cooldown = 0.5f;

    [Header("Black Hole Parameters")]
    public int max_black_holes = 3;
    public int current_black_holes = 3;
    public GameObject blackhole_prefab;
    public float bInstance_dist = 1.5f;
    public float blackhole_activation_time = 3.0f;

    [Header("Move Parameters")]
    public float y_axis;
    public float x_axis;
    public float brake_power = 2.0f;

    [Header("Cursor Parameters")]
    public Texture2D cursorTextureIdle;
    public Texture2D cursorTextureClick;
    public Vector2 offset;
    public float texture_cooldown = 0.1f;

    // Start is called before the first frame update
    private void Start()
    {
        base.Start();
        offset += new Vector2(8,8);
        Cursor.SetCursor(cursorTextureIdle, offset, CursorMode.Auto);
        textDelay =  new WaitForSeconds(texture_cooldown);
        missileDelay = new WaitForSeconds(missile_cooldown);
        blackHoleDelay = new WaitForSeconds(blackhole_activation_time);
        current_black_holes = max_black_holes;
    }

    // Update is called once per frame
    private void Update()
    {
        if (Time.timeScale <= 0.0f)
            return;
        // Maybe use this to aim the shots later
        y_axis = Input.GetAxis("Vertical");
        x_axis = Input.GetAxis("Horizontal");

        if ((Input.GetButtonDown("Fire1") || Input.GetButton("Fire1")) && canFire == true)
        {
            canFire = false;
            StartCoroutine(FireMissile());
        }

        if ((Input.GetButtonDown("Fire2") || Input.GetButton("Fire2")) && canBlackHole == true && current_black_holes > 0)
        {
            canBlackHole = false;
            --current_black_holes;
            StartCoroutine(FireBlackHole());
        }

        if (Input.GetButtonDown("Jump") || Input.GetButton("Jump"))
            brakes = true;
        else
            brakes = false;

        PropulsionSystems();

        if (Input.GetKeyDown(KeyCode.KeypadEnter))
            TakeDamage(100);
    }

    // Update is called at a constant rate
    private void FixedUpdate()
    {
        if (brakes)
        {
            Vector2 newVelocity = rb.velocity;
            newVelocity = Vector3.Lerp(newVelocity, Vector3.zero, brake_power * Time.deltaTime);
            rb.velocity = newVelocity;
        }
        else ThrustForward(y_axis);

        Rotate(-x_axis);
    }

    // Other

    public void AddBombs(int n)
    {
        current_black_holes += n;
        current_black_holes = Mathf.Clamp(current_black_holes, 0, max_black_holes);
    }

    IEnumerator ChangeCursor ()
    {
        Cursor.SetCursor(cursorTextureClick, offset, CursorMode.Auto);

        yield return textDelay;

        Cursor.SetCursor(cursorTextureIdle, offset, CursorMode.Auto);
    }

    IEnumerator FireMissile()
    {
        GameObject c = GameObject.Find("Main Camera");
        Camera cam = c.GetComponent<Camera>();
        Vector3 aimDir = cam.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        aimDir.z = 0.0f;
        aimDir.Normalize();

        Vector3 pos = transform.position + aimDir * mInstance_dist;
        GameObject missile = Instantiate(missile_prefab, pos, transform.rotation);
        AudioManager.PlayShot();

        missile.transform.up = aimDir;

        StartCoroutine(ChangeCursor());

        yield return missileDelay;

        canFire = true;
    }

    IEnumerator FireBlackHole()
    {
        Vector3 pos = transform.position - transform.up * bInstance_dist;
        GameObject bh = Instantiate(blackhole_prefab, pos, Quaternion.identity);

        bh.GetComponentInChildren<BombBehaviour>().activation_time = blackhole_activation_time;
        bh.GetComponentInChildren<BombBehaviour>().activated = true;

        yield return blackHoleDelay;

        canBlackHole = true;
    }

    protected new void PropulsionSystems()
    {
        // Modify propulsion particle emitters
        ParticleSystem.MainModule p1 = props[0].main;
        ParticleSystem.MainModule p2 = props[1].main;
        ParticleSystem.MainModule p3 = props[2].main;
        ParticleSystem.MainModule p4 = props[3].main;

        float forwards = Mathf.Clamp(y_axis, 0.0f, 1.0f);
        p1.startSize = idle_start_size + forwards * (max_start_size - idle_start_size);
        p1.startLifetime = idle_life_time + forwards * (max_life_time - idle_life_time);
        p2.startSize = idle_start_size + forwards * (max_start_size - idle_start_size);
        p2.startLifetime = idle_life_time + forwards * (max_life_time - idle_life_time);

        float backwards = Mathf.Clamp(y_axis, -1.0f, 0.0f);
        p3.startSize = idle_start_size - backwards * (max_start_size - idle_start_size);
        p3.startLifetime = idle_life_time - backwards * (max_life_time - idle_life_time);
        p4.startSize = idle_start_size - backwards * (max_start_size - idle_start_size);
        p4.startLifetime = idle_life_time - backwards * (max_life_time - idle_life_time);

        if (brakes)
        {
            p1.startSize = idle_start_size + (max_start_size - idle_start_size);
            p1.startLifetime = idle_life_time + (max_life_time - idle_life_time);
            p2.startSize = idle_start_size + (max_start_size - idle_start_size);
            p2.startLifetime = idle_life_time + (max_life_time - idle_life_time);

            p3.startSize = idle_start_size + (max_start_size - idle_start_size);
            p3.startLifetime = idle_life_time + (max_life_time - idle_life_time);
            p4.startSize = idle_start_size + (max_start_size - idle_start_size);
            p4.startLifetime = idle_life_time + (max_life_time - idle_life_time);
        }
    }
}
