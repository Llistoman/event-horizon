﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface ShipInterface
{
    Rigidbody2D rb { get; set; }
    ParticleSystem[] props { get; set; }
    float current_speed { get; set; }
    bool current_direction { get; set; }
    bool previous_direction { get; set; }
    float signed_vel1 { get; set; }
    float speed { get; set; }
    float max_speed { get; set; }
    float rotation_speed { get; set; } // degrees/s
    float hp { get; set; }
    float max_hp { get; set; }
    float score { get; set; }
    float chance_health { get; set; }
    float chance_bomb { get; set; }
    GameObject health { get; set; }
    GameObject bomb { get; set; }

    float idle_start_size { get; set; }
    float idle_life_time { get; set; }
    float max_start_size { get; set; }
    float max_life_time { get; set; }

    void Start();
    void ClampVelocity();
    void ThrustForward(float ammount);
    void ThrustBackward(float ammount);
    void Rotate(float ammount);
    void RotateTowardsPos(Vector3 pos);
    void PropulsionSystems();
    void TakeDamage(float dmg);
    void DestroyShip();
}

public abstract class ShipBase : MonoBehaviour, ShipInterface
{
    public Rigidbody2D rb { get; set; }
    public ParticleSystem[] props { get; set; }

    [Header("Ship Parameters")]
    public float _hp = 100.0f;
    public float hp { get { return _hp; } set { _hp = value; } } // this can be set from the editor
    public float _max_hp;
    public float max_hp { get { return _max_hp; } set { _max_hp = value; } }
    public float _score;
    public float score { get { return _score; } set { _score = value; } }
    public float _chance_health;
    public float chance_health { get { return _chance_health; } set { _chance_health = value; } }
    public float _chance_bomb;
    public float chance_bomb { get { return _chance_bomb; } set { _chance_bomb = value; } }
    public GameObject _health;
    public GameObject health { get { return _health; } set { _health = value; } }
    public GameObject _bomb;
    public GameObject bomb { get { return _bomb; } set { _bomb = value; } }

    [Header("Move Parameters")]
    public float _speed = 4.0f;
    public float speed { get { return _speed; } set { _speed = value; } }
    public float _max_speed = 4.0f;
    public float max_speed { get { return _max_speed; } set { _max_speed = value; } }
    public float _rotation_speed = 360.0f; // degrees/s
    public float rotation_speed { get { return _rotation_speed; } set { _rotation_speed = value; } }
    public float current_speed { get; set; }
    public float signed_vel1 { get; set; }
    public bool current_direction { get; set; }
    public bool previous_direction { get; set; }

    [Header("Propulsion Particles Parameters")]
    public float _idle_start_size = 0.0f;
    public float idle_start_size { get { return _idle_start_size; } set { _idle_start_size = value; } }
    public float _idle_life_time = 0.0f;
    public float idle_life_time { get { return _idle_life_time; } set { _idle_life_time = value; } }
    public float _max_start_size = 0.35f;
    public float max_start_size { get { return _max_start_size; } set { _max_start_size = value; } }
    public float _max_life_time = 0.7f;
    public float max_life_time { get { return _max_life_time; } set { _max_life_time = value; } }

    // Start is called before the first frame update
    public void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        props = GetComponentsInChildren<ParticleSystem>();
        foreach (ParticleSystem p in props)
        {
            ParticleSystem.MainModule prop = p.main;
            prop.startSize = idle_start_size;
            prop.startLifetime = idle_life_time;
        }
        max_hp = hp;
    }

    public void ClampVelocity()
    {
        Vector2 newVelocity = rb.velocity;
        newVelocity.x = Mathf.Clamp(rb.velocity.x, -max_speed, max_speed);
        newVelocity.y = Mathf.Clamp(rb.velocity.y, -max_speed, max_speed);

        rb.velocity = newVelocity;
    }

    public void ThrustForward(float ammount)
    {
        Vector2 force = transform.up * ammount * speed;

        rb.AddForce(force);
        ClampVelocity();
        current_speed = rb.velocity.magnitude;
    }

    public void ThrustBackward(float ammount)
    {
        Vector2 force = transform.up * ammount * speed;

        rb.AddForce(-force);
        ClampVelocity();
    }

    public void Rotate(float ammount)
    {
        float new_angle = ammount * rotation_speed * Time.deltaTime;

        rb.MoveRotation(rb.rotation + new_angle);
    }

    public void RotateTowardsPos(Vector3 pos)
    {
        Vector3 dir2Pos = pos - transform.position;
        dir2Pos.z = 0.0f;
        dir2Pos.Normalize();

        float new_angle = Vector3.SignedAngle(transform.up, dir2Pos, transform.forward);

        rb.MoveRotation(rb.rotation + new_angle * rotation_speed * Time.deltaTime);
    }

    public void PropulsionSystems()
    {

    }

    public void TakeDamage(float dmg)
    {
        hp -= dmg;
        if (hp <= 0.0f)
            DestroyShip();
        else
            hp = Mathf.Clamp(hp, 0.0f, max_hp);
    }

    public void DestroyShip()
    {
        if (gameObject.tag == "Player")
        {
            gameObject.transform.root.gameObject.SetActive(false);
        }
        else
        {
            Debug.Log("base");
            GameUI.score += score;

            float roll = Random.Range(0, 100);
            if (roll <= chance_health)
            {
                GameObject h = Instantiate(health, transform.position, Quaternion.identity);
                h.GetComponentInChildren<Rigidbody2D>().AddForce(new Vector2(Random.Range(0, 1), Random.Range(0, 1)));
            }
            if (roll <= _chance_bomb)
            {
                GameObject b = Instantiate(bomb, transform.position, Quaternion.identity);
                b.GetComponentInChildren<Rigidbody2D>().AddForce(new Vector2(Random.Range(0, 1), Random.Range(0, 1)));
            }

            Destroy(gameObject.transform.root.gameObject);
        }
    }
}
