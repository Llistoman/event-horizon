﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance = null;

    public AudioSource _audioMusic;
    public AudioSource _audioSFX;
    public AudioClip _music;
    public AudioClip _shot;
    public AudioClip _laser;
    public static AudioSource audioMusic;
    public static AudioSource audioSFX;
    public static AudioClip music;
    public static AudioClip shot;
    public static AudioClip laser;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);

        audioMusic = _audioMusic;
        audioSFX = _audioSFX;
        music = _music;
        shot = _shot;
        laser = _laser;

        PlayMusic();
    }

    public static void PlayMusic()
    {
        audioMusic.clip = music;
        audioMusic.loop = true;
        audioMusic.Play();
    }

    public static void StopMusic()
    {
        audioMusic.loop = false;
        audioMusic.Stop();
    }

    public static void PlayShot()
    {
        audioSFX.PlayOneShot(shot);
    }

    public static void PlayLaser()
    {
        audioSFX.PlayOneShot(laser);
    }

    public static float GetVolume()
    {
        return audioMusic.volume;
    }

    public static void SetVolume(float vol)
    {
        audioMusic.volume = vol;
        audioSFX.volume = vol;
    }

}
