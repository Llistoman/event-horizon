﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    [Header("Round Parameters")]
    public int roundsToWin = 3;
    public float roundStartDelay = 3.0f;
    public float roundEndDelay = 3.0f;
    public static int startingEnemies = 4;
    public static int enemiesPerRound = 2;
    public static int bossPerRound = 1;
    public float delayBetweenSpawns = 2.0f;
    public float spawnOffset;

    [Header("Enemy Prefabs")]
    public GameObject fighter1;
    public GameObject boss1;
    public GameObject fighter2;
    public GameObject boss2;
    public GameObject fighter3;
    public GameObject boss3;

    [Header("UI Messages")]
    public GameObject roundStartText;
    public GameObject roundClearText;
    public GameObject WinMenu;
    public GameObject FadeObject;

    [Header("State Parameters")]
    public static bool playerHasWon = false;
    public static bool playerHasLost = false;
    public static int round_enemies_killed = 0;
    public static int round_enemies_to_kill = 0;
    public static int round_boss_killed = 0;
    public static int round_boss_to_kill = 0;
    public static int current_round = 0;

    // Private
    private int current_enemies_spawned = 0;
    private WaitForSeconds startDelay;
    private WaitForSeconds endDelay;
    private WaitForSeconds spawnDelay;
    private GameObject player;

    /*public int m_NumRoundsToWin = 5;            // The number of rounds a single player has to win to win the game.
    public float m_StartDelay = 3f;             // The delay between the start of RoundStarting and RoundPlaying phases.
    public float m_EndDelay = 3f;               // The delay between the end of RoundPlaying and RoundEnding phases.
    public CameraControl m_CameraControl;       // Reference to the CameraControl script for control during different phases.
    public Text m_MessageText;                  // Reference to the overlay Text to display winning text, etc.
    public GameObject m_TankPrefab;             // Reference to the prefab the players will control.
    public TankManager[] m_Tanks;               // A collection of managers for enabling and disabling different aspects of the tanks.

    private int m_RoundNumber;                  // Which round the game is currently on.
    private WaitForSeconds m_StartWait;         // Used to have a delay whilst the round starts.
    private WaitForSeconds m_EndWait;           // Used to have a delay whilst the round or game ends.
    private TankManager m_RoundWinner;          // Reference to the winner of the current round.  Used to make an announcement of who won.
    private TankManager m_GameWinner;           // Reference to the winner of the game.  Used to make an announcement of who won.*/

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        startDelay = new WaitForSeconds(roundStartDelay);
        endDelay = new WaitForSeconds(roundEndDelay);
        spawnDelay = new WaitForSeconds(delayBetweenSpawns);

        player = GameObject.Find("PlayerShip");

        RestartGame();

        // Start the game.
        StartCoroutine(GameLoop());
    }

    public static void RestartGame()
    {
        GameUI.score = 0.0f;
        current_round = 0;
        playerHasWon = false;
        playerHasLost = false;
        round_enemies_killed = 0;
        round_boss_killed = 0;

        round_enemies_to_kill = startingEnemies;
        round_boss_to_kill = bossPerRound;
    }

    private void Update()
    {
        //Debug.Log("Round: " + current_round + ", Enemies: " + round_enemies_killed + "/" + round_enemies_to_kill + ", Boss: " + round_boss_killed + "/" + round_boss_to_kill);
    }

    // This is called from start and will run each phase of the game one after another.
    private IEnumerator GameLoop()
    {
        // Wait for fade from black
        yield return StartCoroutine(FadeFromBlack(1.0f));

        while (current_round < roundsToWin)
        {
            // Start off by running the 'RoundStarting' coroutine but don't return until it's finished.
            yield return StartCoroutine(RoundStarting());

            // Once the 'RoundStarting' coroutine is finished, run the 'RoundPlaying' coroutine but don't return until it's finished.
            yield return StartCoroutine(RoundPlaying());

            // Once execution has returned here, run the 'RoundEnding' coroutine, again don't return until it's finished.
            yield return StartCoroutine(RoundEnding());

            ++current_round;
        }
        // Once execution is here, player has won the game
        WinGame();
    }


    private IEnumerator RoundStarting()
    {
        switch (current_round)
        {
            case 0:
                roundStartText.GetComponent<TextMeshProUGUI>().text = "ROUND 1 START!";
                break;
            case 1:
                roundStartText.GetComponent<TextMeshProUGUI>().text = "ROUND 2 START!";
                break;
            case 2:
                roundStartText.GetComponent<TextMeshProUGUI>().text = "ROUND 3 START!";
                break;
        }
        roundStartText.SetActive(true);

        yield return startDelay;

        roundStartText.SetActive(false);
    }


    private IEnumerator RoundPlaying()
    {
        // start spawning round enemies
        StartCoroutine(SpawnEnemies());

        // wait for all enemies to be dead
        while (round_enemies_killed < round_enemies_to_kill)
        {
            yield return null;
        }

        // Spawn Boss
        switch (current_round)
        {
            case 0:
                SpawnEnemy(boss1);
                break;
            case 1:
                SpawnEnemy(boss2);
                break;
            case 2:
                SpawnEnemy(boss3);
                break;
        }

        // wait for boss to be dead
        while (round_boss_killed < round_boss_to_kill)
        {
            yield return null;
        }
    }

    private IEnumerator SpawnEnemies()
    {
        while (current_enemies_spawned < round_enemies_to_kill)
        {
            switch (current_round)
            {
                case 0:
                    SpawnEnemy(fighter1);
                    break;
                case 1:
                    SpawnEnemy(fighter2);
                    break;
                case 2:
                    SpawnEnemy(fighter3);
                    break;
            }
            ++current_enemies_spawned;
            yield return spawnDelay;
        }
        current_enemies_spawned = 0;
    }

    private void SpawnEnemy(GameObject enemy)
    {
        Vector3 playerPos = player.transform.position;
        Vector3 spawnPos = playerPos;
        int dir = Random.Range(0, 4);
        if (dir == 0)
            spawnPos += new Vector3(0, spawnOffset);
        if (dir == 1)
            spawnPos += new Vector3(spawnOffset, 0);
        if (dir == 2)
            spawnPos += new Vector3(0, -spawnOffset);
        if (dir == 3 || dir == 4)
            spawnPos += new Vector3(-spawnOffset, 0);

        Instantiate(enemy, spawnPos, Quaternion.identity);
    }


    private IEnumerator RoundEnding()
    {
        roundClearText.SetActive(true);
        yield return endDelay;
        roundClearText.SetActive(false);
        round_enemies_to_kill += enemiesPerRound;
        round_boss_to_kill = bossPerRound;
        round_enemies_killed = 0;
        round_boss_killed = 0;
    }

    private void WinGame()
    {
        playerHasWon = true;
        Time.timeScale = 0.0f;
        WinMenu.SetActive(true);
    }

    public IEnumerator FadeIntoBlack(float time)
    {
        instance.FadeObject.GetComponent<Image>().CrossFadeAlpha(1, time, true);
        // Wait for fade to black
        yield return new WaitForSeconds(time);
    }

    public IEnumerator FadeFromBlack(float time)
    {
        instance.FadeObject.GetComponent<Image>().color = Color.black;
        instance.FadeObject.GetComponent<Image>().CrossFadeAlpha(0, time, true);
        // Wait for fade to black
        yield return new WaitForSeconds(time);
    }
}