﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour
{
    public GameObject GameOverUI;

    private GameObject player;

    // Start is called before the first frame update
    private void Start()
    {
        player = GameObject.Find("PlayerShip");
    }

    // Update is called once per frame
    private void Update()
    {
        if (player.GetComponent<PlayerInput>().hp <= 0.0f)
        {
            GameOverUI.SetActive(true);
            GameManager.playerHasLost = true;
        }
    }

    public void Restart()
    {
        Time.timeScale = 1.0f;
        StartCoroutine(FadeAndRestart());
    }

    public void LoadMenu()
    {
        Time.timeScale = 1.0f;
        StartCoroutine(FadeAndMenu());
    }

    public void QuitGame()
    {
        Time.timeScale = 1.0f;
        StartCoroutine(FadeAndQuit());
    }

    private IEnumerator FadeAndQuit()
    {
        yield return StartCoroutine(GameManager.instance.FadeIntoBlack(1.0f));
        Application.Quit();
    }

    private IEnumerator FadeAndMenu()
    {
        yield return StartCoroutine(GameManager.instance.FadeIntoBlack(1.0f));
        SceneManager.LoadScene("MainMenu");
    }

    private IEnumerator FadeAndRestart()
    {
        yield return StartCoroutine(GameManager.instance.FadeIntoBlack(1.0f));
        SceneManager.LoadScene("MainLevel");
    }
}
