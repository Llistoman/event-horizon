﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;
    public GameObject PauseMenuUI;

    private float timescale;

    // Start is called before the first frame update
    private void Start()
    {

    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !GameManager.playerHasWon && !GameManager.playerHasLost)
        {
            if (GameIsPaused)
                Resume();
            else
                Pause();
        }
    }

    private void Pause()
    {
        PauseMenuUI.SetActive(true);
        timescale = Time.timeScale;
        Time.timeScale = 0.0f;
        GameIsPaused = true;
    }

    public void Resume()
    {
        PauseMenuUI.SetActive(false);
        Time.timeScale = timescale;
        GameIsPaused = false;
    }

    public void Restart()
    {
        Time.timeScale = 1.0f;
        StartCoroutine(FadeAndRestart());
    }

    public void LoadMenu()
    {
        Time.timeScale = 1.0f;
        StartCoroutine(FadeAndMenu());
    }

    public void QuitGame()
    {
        Time.timeScale = 1.0f;
        StartCoroutine(FadeAndQuit());
    }

    private IEnumerator FadeAndQuit()
    {
        yield return StartCoroutine(GameManager.instance.FadeIntoBlack(1.0f));
        GameIsPaused = false;
        Application.Quit();
    }

    private IEnumerator FadeAndMenu()
    {
        yield return StartCoroutine(GameManager.instance.FadeIntoBlack(1.0f));
        GameIsPaused = false;
        SceneManager.LoadScene("MainMenu");
    }

    private IEnumerator FadeAndRestart()
    {
        yield return StartCoroutine(GameManager.instance.FadeIntoBlack(1.0f));
        GameIsPaused = false;
        SceneManager.LoadScene("MainLevel");
    }
}
