﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject tutorial;
    public GameObject VolumeSlider;
    public GameObject FadeObject;

    private bool tutorialState = true;

    // Start is called before the first frame update
    void Start()
    {
        InitialVolume();
        StartCoroutine(FadeFromBlack(1.0f));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame()
    {
        Time.timeScale = 1.0f;
        StartCoroutine(FadeAndStart());
    }

    public void ShowTutorial()
    {
        if (tutorialState)
            tutorial.SetActive(true);
        else
            tutorial.SetActive(false);
        tutorialState = !tutorialState;
    }

    private void InitialVolume()
    {
        float val = AudioManager.GetVolume();
        Slider slider = VolumeSlider.GetComponent<Slider>();
        slider.value = val;
        AudioManager.SetVolume(val);
    }

    public void ChangeVolume()
    {
        Slider slider = VolumeSlider.GetComponent<Slider>();
        AudioManager.SetVolume(slider.value);
    }

    public void QuitGame()
    {
        Time.timeScale = 1.0f;
        StartCoroutine(FadeAndQuit());
    }

    private IEnumerator FadeAndQuit()
    {
        yield return StartCoroutine(FadeIntoBlack(1.0f));
        Application.Quit();
    }

    private IEnumerator FadeAndStart()
    {
        yield return StartCoroutine(FadeIntoBlack(1.0f));
        SceneManager.LoadScene("MainLevel");
    }

    public IEnumerator FadeIntoBlack(float time)
    {
        FadeObject.GetComponent<Image>().CrossFadeAlpha(1, time, true);
        // Wait for fade to black
        yield return new WaitForSeconds(time);
    }

    public IEnumerator FadeFromBlack(float time)
    {
        FadeObject.GetComponent<Image>().color = Color.black;
        FadeObject.GetComponent<Image>().CrossFadeAlpha(0, time, true);
        // Wait for fade to black
        yield return new WaitForSeconds(time);
    }
}

