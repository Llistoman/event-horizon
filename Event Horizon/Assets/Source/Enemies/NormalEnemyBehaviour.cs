﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalEnemyBehaviour : EnemyBase, ShipInterface
{
    // Because C# doesn't support multiple inheritance or default interface implementations (not yet at least) we'll have to reimplement 
    // one of the interfaces and just inherit the base class of the other, I chose the ShipInterface because it's shorter. 
    // We could reimplement both but that's extra redundant work.

    public Rigidbody2D rb { get; set; }
    public ParticleSystem[] props { get; set; }

    [Header("Ship Parameters")]
    public float _hp = 100.0f;
    public float hp { get { return _hp; } set { _hp = value; } } // this can be set from the editor
    public float _max_hp;
    public float max_hp { get { return _max_hp; } set { _max_hp = value; } }
    public float _score;
    public float score { get { return _score; } set { _score = value; } }
    public float _chance_health;
    public float chance_health { get { return _chance_health; } set { _chance_health = value; } }
    public float _chance_bomb;
    public float chance_bomb { get { return _chance_bomb; } set { _chance_bomb = value; } }
    public GameObject _health;
    public GameObject health { get { return _health; } set { _health = value; } }
    public GameObject _bomb;
    public GameObject bomb { get { return _bomb; } set { _bomb = value; } }

    [Header("Move Parameters")]
    public float _speed = 4.0f;
    public float speed { get { return _speed; } set { _speed = value; } }
    public float _max_speed = 4.0f;
    public float max_speed { get { return _max_speed; } set { _max_speed = value; } }
    public float _rotation_speed = 360.0f; // degrees/s
    public float rotation_speed { get { return _rotation_speed; } set { _rotation_speed = value; } }
    public float current_speed { get; set; }
    public float signed_vel1 { get; set; }
    public bool current_direction { get; set; }
    public bool previous_direction { get; set; }

    [Header("Propulsion Particles Parameters")]
    public float _idle_start_size = 0.0f;
    public float idle_start_size { get { return _idle_start_size; } set { _idle_start_size = value; } }
    public float _idle_life_time = 0.0f;
    public float idle_life_time { get { return _idle_life_time; } set { _idle_life_time = value; } }
    public float _max_start_size = 0.35f;
    public float max_start_size { get { return _max_start_size; } set { _max_start_size = value; } }
    public float _max_life_time = 0.7f;
    public float max_life_time { get { return _max_life_time; } set { _max_life_time = value; } }

    // Start is called before the first frame update
    public new void Start()
    {
        base.Start();
        rb = GetComponent<Rigidbody2D>();
        props = GetComponentsInChildren<ParticleSystem>();
        foreach (ParticleSystem p in props)
        {
            ParticleSystem.MainModule prop = p.main;
            prop.startSize = idle_start_size;
            prop.startLifetime = idle_life_time;
        }
        current_direction = true;
        previous_direction = true;
        max_hp = hp;
    }

    // Update is called once per frame
    private void Update()
    {
        PredictPlayerPos();
        if (PosInRange(playerPredictedPos) && PosInRange(playerPos) && PosInFront(playerPredictedPos) && canShoot)
        {
            StartCoroutine(ShootBurst());
        }
        if (hp > 0.0f)
            PropulsionSystems();
    }

    private void FixedUpdate()
    {
        if (PosInMinRange(playerPos))
        {
            current_direction = false;
            ThrustBackward(1);
            RotateTowardsPos(playerPredictedPos);
        }
        else if (!PosInRange(playerPos))
        {
            current_direction = true;
            ThrustForward(1);
            RotateTowardsPos(playerPos);
        }
        else
        {
            RotateTowardsPos(playerPredictedPos);
        }
        previous_direction = current_direction;
    }

    public void ClampVelocity()
    {
        Vector2 newVelocity = rb.velocity;
        newVelocity.x = Mathf.Clamp(rb.velocity.x, -max_speed, max_speed);
        newVelocity.y = Mathf.Clamp(rb.velocity.y, -max_speed, max_speed);

        rb.velocity = newVelocity;

        //map(value, low1, high1, low2, high2)
        //low2 + (value - low1) * (high2 - low2) / (high1 - low1)
        signed_vel1 = -1 + (rb.velocity.magnitude - (-max_speed)) * (1.0f - (-1.0f)) / (max_speed - (-max_speed));
    }

    public void ThrustForward(float ammount)
    {
        Vector2 force = transform.up * ammount * speed;

        rb.AddForce(force);
        ClampVelocity();
        current_speed = rb.velocity.magnitude;
    }

    public void ThrustBackward(float ammount)
    {
        Vector2 force = transform.up * ammount * speed;

        rb.AddForce(-force);
        ClampVelocity();
        current_speed = rb.velocity.magnitude;
    }

    public void Rotate(float ammount)
    {
        float new_angle = ammount * rotation_speed * Time.deltaTime;

        rb.MoveRotation(rb.rotation + new_angle);
    }

    public void RotateTowardsPos(Vector3 pos)
    {
        Vector3 dir2Pos = pos - transform.position;
        dir2Pos.z = 0.0f;
        dir2Pos.Normalize();

        float real_angle = Vector3.SignedAngle(transform.up, dir2Pos, transform.forward);

        //map(value, low1, high1, low2, high2)
        //low2 + (value - low1) * (high2 - low2) / (high1 - low1)
        float ammount = -1.0f + (real_angle - (-180.0f)) * (1.0f - (-1.0f)) / (180.0f - (-180.0f));

        float new_angle = ammount * rotation_speed * Time.deltaTime;

        rb.MoveRotation(rb.rotation + new_angle);
    }

    public void PropulsionSystems()
    {
        // Modify propulsion particle emitters
        ParticleSystem.MainModule p1 = props[0].main;
        ParticleSystem.MainModule p2 = props[1].main;
        ParticleSystem.MainModule p3 = props[2].main;

        if (PosInRange(playerPos))
        {
            p1.startSize = 0.0f;
            p1.startLifetime = 0.0f;
            p2.startSize = 0.0f;
            p2.startLifetime = 0.0f;
            p3.startSize = 0.0f;
            p3.startLifetime = 0.0f;
        }
        else if (current_direction == true && previous_direction == true)
        {
            p1.startSize = idle_start_size + signed_vel1 * (max_start_size - idle_start_size);
            p1.startLifetime = idle_life_time + signed_vel1 * (max_life_time - idle_life_time);
            p2.startSize = 0.0f;
            p2.startLifetime = 0.0f;
            p3.startSize = 0.0f;
            p3.startLifetime = 0.0f;
        }
        else if (current_direction == false && previous_direction == false)
        {
            p1.startSize = 0.0f;
            p1.startLifetime = 0.0f;
            p2.startSize = idle_start_size + Mathf.Abs(signed_vel1) * (max_start_size - idle_start_size);
            p2.startLifetime = idle_life_time + Mathf.Abs(signed_vel1) * (max_life_time - idle_life_time);
            p3.startSize = idle_start_size + Mathf.Abs(signed_vel1) * (max_start_size - idle_start_size);
            p3.startLifetime = idle_life_time + Mathf.Abs(signed_vel1) * (max_life_time - idle_life_time);
        }
    }

    public void TakeDamage(float dmg)
    {
        hp -= dmg;
        if (hp <= 0.0f)
        {
            GameUI.score += score;
            ParticleSystem.MainModule p1 = props[0].main;
            ParticleSystem.MainModule p2 = props[1].main;
            ParticleSystem.MainModule p3 = props[2].main;
            p1.startSize = 0.0f;
            p1.startLifetime = 0.0f;
            p2.startSize = 0.0f;
            p2.startLifetime = 0.0f;
            p3.startSize = 0.0f;
            p3.startLifetime = 0.0f;

            Canvas c = gameObject.transform.root.gameObject.GetComponentInChildren<Canvas>();
            c.enabled = false;

            ++GameManager.round_enemies_killed;

            // Instantiate power ups if needed
            Collider2D myCol = GetComponent<Collider2D>();
            myCol.enabled = false;
            float roll = Random.Range(0, 100);
            if (roll <= chance_health)
            {
                GameObject h = Instantiate(health, transform.position + new Vector3(0.5f, 0.5f), Quaternion.identity);
                h.GetComponentInChildren<Rigidbody2D>().AddForce(new Vector2(Random.Range(1, 2), Random.Range(1, 2)));
            }
            if (roll <= _chance_bomb)
            {
                GameObject b = Instantiate(bomb, transform.position + new Vector3(-0.5f, -0.5f), Quaternion.identity);
                b.GetComponentInChildren<Rigidbody2D>().AddForce(new Vector2(Random.Range(1, 2), Random.Range(1, 2)));
            }

            Animator[] animators = gameObject.transform.root.gameObject.GetComponentsInChildren<Animator>();
            foreach (Animator anim in animators)
            {
                anim.SetBool("Destroyed", true);
            }

            StopAllCoroutines();
            StartCoroutine(DestroyAfterAnimation());
        }
        hp = Mathf.Clamp(hp, 0.0f, max_hp);
    }

    public new IEnumerator DestroyAfterAnimation()
    {
        Animator a = gameObject.GetComponent<Animator>();
        while (a.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            // Aparently at the start we are still not in the explosion animation
            yield return null;
        }

        while (a.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.99f)
        {
            yield return null;
        }
        DestroyShip();
    }

    public void DestroyShip()
    {
        Destroy(gameObject.transform.root.gameObject);
    }
}
