﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBars : MonoBehaviour
{
    public GameObject obj;

    private ShipInterface i;
    private float max_health;
    private Image img;

    // Start is called before the first frame update
    void Start()
    {
        i = obj.transform.root.gameObject.GetComponentInChildren<ShipInterface>();
        max_health = i.hp;
        img = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        img.fillAmount = i.hp / max_health;
    }
}
