﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface EnemyInterface
{
    int shots_in_burst { get; set; }
    float delay_shot_time { get; set; }
    float delay_burst_time { get; set; }
    float shooting_angle { get; set; }
    float min_dist_shoot { get; set; }
    float max_dist_shoot { get; set; }
    float projectile_offset { get; set; }
    GameObject projectile_prefab { get; set; }

    float laser_damage_per_tick { get; set; }
    float laser_preparation_time { get; set; }
    float laser_active_time { get; set; }
    float laser_chance { get; set; }
    float laser_roll { get; set; }
    float laser_cooldown { get; set; }
    float laser_speed { get; set; }
    float laser_check_time { get; set; }
    List<GameObject> pre_lasers { get; set; }
    List<GameObject> post_lasers { get; set; }

    GameObject player { get; set; }
    Vector3 playerVelocity { get; set; }
    Vector3 playerPos { get; set; }
    Vector3 playerPredictedPos { get; set; }
    Vector3 dir2PlayerPredicted { get; set; }
    bool canShoot { get; set; }
    WaitForSeconds shotDelay { get; set; }
    WaitForSeconds burstDelay { get; set; }
    int shots_fired { get; set; }
    bool canLaser { get; set; }
    WaitForSeconds laserPreparation { get; set; }
    WaitForSeconds laserCooldown { get; set; }
    WaitForSeconds laserCheckTIme { get; set; }

    void Start();
    IEnumerator ShootBurst();
    IEnumerator DelayBurst();
    IEnumerator DestroyAfterAnimation();
    bool PosInFront(Vector3 pos);
    bool PosInRange(Vector3 pos);
    bool PosInMinRange(Vector3 pos);
    void PredictPlayerPos();
    Vector3 FirstOrderIntercept(Vector3 shooterPosition, Vector3 shooterVelocity, float shotSpeed, Vector3 targetPosition, Vector3 targetVelocity);
    void OnDrawGizmos();
}

public class EnemyBase : MonoBehaviour, EnemyInterface
{
    [Header("Shot Parameters")]
    public int _shots_in_burst = 3;
    public int shots_in_burst { get { return _shots_in_burst; } set { _shots_in_burst = value; } }
    public float _delay_shot_time = 0.25f;
    public float delay_shot_time { get { return _delay_shot_time; } set { _delay_shot_time = value; } }
    public float _delay_burst_time = 2.0f;
    public float delay_burst_time { get { return _delay_burst_time; } set { _delay_burst_time = value; } }
    public float _shooting_angle = 15.0f;
    public float shooting_angle { get { return _shooting_angle; } set { _shooting_angle = value; } }
    public float _min_dist_shoot = 2.0f;
    public float min_dist_shoot { get { return _min_dist_shoot; } set { _min_dist_shoot = value; } }
    public float _max_dist_shoot = 8.0f;
    public float max_dist_shoot { get { return _max_dist_shoot; } set { _max_dist_shoot = value; } }
    public float _projectile_offset = 0.3f;
    public float projectile_offset { get { return _projectile_offset; } set { _projectile_offset = value; } }
    public GameObject _projectile_prefab;
    public GameObject projectile_prefab { get { return _projectile_prefab; } set { _projectile_prefab = value; } }

    [Header("Laser Parameters")]
    public float _laser_damage_per_tick = 1.0f;
    public float laser_damage_per_tick { get { return _laser_damage_per_tick; } set { _laser_damage_per_tick = value; } }
    public float _laser_preparation_time = 2.0f;
    public float laser_preparation_time { get { return _laser_preparation_time; } set { _laser_preparation_time = value; } }
    public float _laser_active_time = 3.0f;
    public float laser_active_time { get { return _laser_active_time; } set { _laser_active_time = value; } }
    public float _laser_chance = 20.0f;
    public float laser_chance { get { return _laser_chance; } set { _laser_chance = value; } }
    public float _laser_roll = 0.0f;
    public float laser_roll { get { return _laser_roll; } set { _laser_roll = value; } }
    public float _laser_cooldown = 5.0f;
    public float laser_cooldown { get { return _laser_cooldown; } set { _laser_cooldown = value; } }
    public float _laser_speed = 5.0f;
    public float laser_speed { get { return _laser_speed; } set { _laser_speed = value; } }
    public float _laser_check_time = 3.0f;
    public float laser_check_time { get { return _laser_check_time; } set { _laser_check_time = value; } }
    public List<GameObject> _pre_lasers;
    public List<GameObject> pre_lasers { get { return _pre_lasers; } set { _pre_lasers = value; } }
    public List<GameObject> _post_lasers;
    public List<GameObject> post_lasers { get { return _post_lasers; } set { _post_lasers = value; } }

    public GameObject _player;
    public GameObject player { get { return _player; } set { _player = value; } }
    public Vector3 _playerVelocity;
    public Vector3 playerVelocity { get { return _playerVelocity; } set { _playerVelocity = value; } }
    public Vector3 _playerPos;
    public Vector3 playerPos { get { return _playerPos; } set { _playerPos = value; } }
    public Vector3 _playerPredictedPos;
    public Vector3 playerPredictedPos { get { return _playerPredictedPos; } set { _playerPredictedPos = value; } }
    public Vector3 _dir2PlayerPredicted;
    public Vector3 dir2PlayerPredicted { get { return _dir2PlayerPredicted; } set { _dir2PlayerPredicted = value; } }

    public bool _canShoot = true;
    public bool canShoot { get { return _canShoot; } set { _canShoot = value; } }
    public WaitForSeconds _shotDelay;
    public WaitForSeconds shotDelay { get { return _shotDelay; } set { _shotDelay = value; } }
    public WaitForSeconds _burstDelay;
    public WaitForSeconds burstDelay { get { return _burstDelay; } set { _burstDelay = value; } }
    public int _shots_fired = 0;
    public int shots_fired { get { return _shots_fired; } set { _shots_fired = value; } }
    public bool _canLaser = true;
    public bool canLaser { get { return _canLaser; } set { _canLaser = value; } }
    public WaitForSeconds laserCheckTIme { get; set; }
    public WaitForSeconds laserPreparation { get; set; }
    public WaitForSeconds laserCooldown { get; set; }

    public void Start()
    {
        player = GameObject.Find("PlayerShip");
        shotDelay = new WaitForSeconds(delay_shot_time);
        burstDelay = new WaitForSeconds(delay_burst_time);
        laserPreparation = new WaitForSeconds(laser_preparation_time);
        laserCooldown = new WaitForSeconds(laser_cooldown);
        laserCheckTIme = new WaitForSeconds(laser_check_time);
        playerVelocity = Vector3.zero;
        laser_roll = 101.0f;
    }

    public IEnumerator ShootBurst()
    {
        canShoot = false;
        canLaser = false;
        ++shots_fired;
        Vector3 pos = transform.position + transform.up * projectile_offset;
        GameObject projectile = Instantiate(projectile_prefab, pos, transform.rotation);
        AudioManager.PlayShot();

        projectile.transform.up = transform.up;

        if (shots_fired < shots_in_burst)
        {
            yield return shotDelay;
            StartCoroutine(ShootBurst());
        }
        else
            StartCoroutine(DelayBurst());
    }

    public IEnumerator DelayBurst()
    {
        yield return burstDelay;
        shots_fired = 0;
        canLaser = true;
        canShoot = true;
    }

    public IEnumerator CheckLaser()
    {
        yield return laserCheckTIme;
        laser_roll = Random.Range(0.0f, 100.0f);
    }

    public IEnumerator ShootLaser()
    {
        canShoot = false;
        canLaser = false;

        foreach (GameObject preLas in pre_lasers)
        {
            preLas.gameObject.SetActive(true);
        }
        Vector3 scale = transform.GetChild(4).transform.localScale;
        Vector3 orig_scale = scale;
        float t = laser_active_time;

        yield return laserPreparation;

        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.freezeRotation = true;

        foreach (GameObject preLas in pre_lasers)
        {
            preLas.gameObject.SetActive(false);
        }

        AudioManager.PlayLaser();
        while (t >= 0.0f)
        {
            foreach (GameObject postLas in post_lasers)
            {
                postLas.gameObject.SetActive(true);
                postLas.GetComponent<LaserBehaviour>().damage_per_tick = laser_damage_per_tick;
                scale.x += Time.deltaTime * laser_speed;
                postLas.transform.localScale = scale;
            }
            t -= Time.deltaTime;
            yield return null;
        }
        rb.freezeRotation = false;

        foreach (GameObject postLas in post_lasers)
        {
            postLas.transform.localScale = orig_scale;
            postLas.gameObject.SetActive(false);
        }
        StartCoroutine(CooldownLaser());
    }

    public IEnumerator CooldownLaser()
    {
        yield return laserCooldown;
        canShoot = true;
        canLaser = true;
    }

    public bool PosInFront(Vector3 pos)
    {
        Vector3 dir2pos = pos - transform.position;
        dir2pos.z = 0.0f;
        dir2pos.Normalize();

        float angle = Vector3.Angle(transform.up, dir2pos);

        if (angle <= shooting_angle / 2.0f)
            return true;
        else
            return false;
    }

    public bool PosInRange(Vector3 pos)
    {
        if (Vector3.Distance(transform.position, pos) <= max_dist_shoot
            && Vector3.Distance(transform.position, pos) >= min_dist_shoot)
            return true;
        else
            return false;
    }

    public bool PosInMinRange(Vector3 pos)
    {
        if (Vector3.Distance(transform.position, pos) <= min_dist_shoot)
            return true;
        else
            return false;
    }

    public void PredictPlayerPos()
    {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        Rigidbody2D playerRb = player.GetComponent<Rigidbody2D>();
        playerVelocity = playerRb.velocity;
        Vector3 myVelocity = rb.velocity;
        MissileBehaviour projectileBehaviour = projectile_prefab.GetComponentInChildren<MissileBehaviour>();

        // Because of the wrapped world, we have to eat this
        float minDist = Mathf.Infinity;
        float dist1 = Vector3.Distance(transform.position, player.transform.position);
        float dist2 = Vector3.Distance(transform.position, player.transform.position - new Vector3(0.0f, 66.0f, 0.0f));
        float dist3 = Vector3.Distance(transform.position, player.transform.position + new Vector3(0.0f, 66.0f, 0.0f));
        float dist4 = Vector3.Distance(transform.position, player.transform.position - new Vector3(66.0f, 0.0f, 0.0f));
        float dist5 = Vector3.Distance(transform.position, player.transform.position + new Vector3(66.0f, 0.0f, 0.0f));
        if (dist1 < minDist)
        {
            minDist = dist1;
            playerPos = player.transform.position;
        }
        if (dist2 < minDist)
        {
            minDist = dist2;
            playerPos = player.transform.position - new Vector3(0.0f, 66.0f, 0.0f);
        }
        if (dist3 < minDist)
        {
            minDist = dist3;
            playerPos = player.transform.position + new Vector3(0.0f, 66.0f, 0.0f);
        }
        if (dist4 < minDist)
        {
            minDist = dist4;
            playerPos = player.transform.position - new Vector3(66.0f, 0.0f, 0.0f);
        }
        if (dist5 < minDist)
        {
            minDist = dist5;
            playerPos = player.transform.position + new Vector3(66.0f, 0.0f, 0.0f);
        }

        playerPredictedPos = FirstOrderIntercept(transform.position, myVelocity, projectileBehaviour.speed, playerPos, playerRb.velocity);
        dir2PlayerPredicted = playerPredictedPos - transform.position;
    }

    //first-order intercept using absolute target position
    public Vector3 FirstOrderIntercept (Vector3 shooterPosition, Vector3 shooterVelocity, float shotSpeed, Vector3 targetPosition, Vector3 targetVelocity)
    {
        Vector3 targetRelativePosition = targetPosition - shooterPosition;
        Vector3 targetRelativeVelocity = targetVelocity - shooterVelocity;
        float t = FirstOrderInterceptTime
        (
            shotSpeed,
            targetRelativePosition,
            targetRelativeVelocity
        );
        return targetPosition + t * (targetRelativeVelocity);
    }
    //first-order intercept using relative target position
    public float FirstOrderInterceptTime
    (
        float shotSpeed,
        Vector3 targetRelativePosition,
        Vector3 targetRelativeVelocity
    )
    {
        float velocitySquared = targetRelativeVelocity.sqrMagnitude;
        if (velocitySquared < 0.001f)
            return 0f;

        float a = velocitySquared - shotSpeed * shotSpeed;

        //handle similar velocities
        if (Mathf.Abs(a) < 0.001f)
        {
            float t = -targetRelativePosition.sqrMagnitude /
            (
                2f * Vector3.Dot
                (
                    targetRelativeVelocity,
                    targetRelativePosition
                )
            );
            return Mathf.Max(t, 0f); //don't shoot back in time
        }

        float b = 2f * Vector3.Dot(targetRelativeVelocity, targetRelativePosition);
        float c = targetRelativePosition.sqrMagnitude;
        float determinant = b * b - 4f * a * c;

        if (determinant > 0f)
        { //determinant > 0; two intercept paths (most common)
            float t1 = (-b + Mathf.Sqrt(determinant)) / (2f * a),
                    t2 = (-b - Mathf.Sqrt(determinant)) / (2f * a);
            if (t1 > 0f)
            {
                if (t2 > 0f)
                    return Mathf.Min(t1, t2); //both are positive
                else
                    return t1; //only t1 is positive
            }
            else
                return Mathf.Max(t2, 0f); //don't shoot back in time
        }
        else if (determinant < 0f) //determinant < 0; no intercept path
            return 0f;
        else //determinant = 0; one intercept path, pretty much never happens
            return Mathf.Max(-b / (2f * a), 0f); //don't shoot back in time
    }

    // Debug Info
    public void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, min_dist_shoot);

        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, max_dist_shoot);

        Vector3 dir = transform.up;
        Vector3 left = Quaternion.AngleAxis(-shooting_angle / 2.0f, transform.forward) * dir;
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, transform.position + left * max_dist_shoot);

        Vector3 right = Quaternion.AngleAxis(shooting_angle / 2.0f, transform.forward) * dir;
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, transform.position + right * max_dist_shoot);

        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(playerPredictedPos, 0.1f);
    }

    public IEnumerator DestroyAfterAnimation()
    {
        yield return null;
    }
}
