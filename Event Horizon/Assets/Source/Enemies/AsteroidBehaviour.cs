﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidBehaviour : ShipBase
{
    public float damage = 10.0f;
    public float damageSelf = 10.0f;

    // Start is called before the first frame update
    private void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    private void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        ShipInterface i = collision.gameObject.GetComponent<ShipInterface>();
        if (i != null)
            i.TakeDamage(damage);

        if (collision.gameObject.tag != "Projectiles")
            TakeDamage(damageSelf);
    }
}
