﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface ProjectileInterface
{
    Rigidbody2D rb { get; set; }
    List<GameObject> extras { get; set; }
    float current_speed { get; set; }
    float speed { get; set; }
    float max_speed { get; set; }
    float explosion_force { get; set; }
    float explosion_radius { get; set; }
    float damage { get; set; }
    float lifetime { get; set; }
    WaitForSeconds lifeTime { get; set; }

    void Start();
    void ClampVelocity();
    void ThrustForward(float ammount);
    void OnCollisionEnter2D(Collision2D col);
    void StartExplosion();
    void DestroyProjectile();
    IEnumerator DestroyAfterAnimation();
    IEnumerator LifeTime();
}

public class ProjectileBase : MonoBehaviour, ProjectileInterface
{
    // Private
    public Rigidbody2D rb { get; set; }
    public List<GameObject> _extras;
    public List<GameObject> extras { get { return _extras; } set { _extras = value; } }

    [Header("Instance Parameters")]
    public float _damage = 5.0f;
    public float damage { get { return _damage; } set { _damage = value; } }
    public float _lifetime = 10.0f;
    public float lifetime { get { return _lifetime; } set { _lifetime = value; } }
    public float _explosion_force = 5.0f;
    public float explosion_force { get { return _explosion_force; } set { _explosion_force = value; } }
    public float _explosion_radius = 0.3f;
    public float explosion_radius { get { return _explosion_radius; } set { _explosion_radius = value; } }
    public float _speed = 6.0f;
    public float speed { get { return _speed; } set { _speed = value; } }
    public float _max_speed = 10.0f;
    public float max_speed { get { return _max_speed; } set { _max_speed = value; } }
    public float _current_speed;
    public float current_speed { get { return _current_speed; } set { _current_speed = value; } }
    public WaitForSeconds lifeTime { get; set; }

    public void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.up * speed;
        lifeTime = new WaitForSeconds(lifetime);
        StartCoroutine(LifeTime());
    }

    public void ClampVelocity()
    {
        Vector2 newVelocity = rb.velocity;
        newVelocity.x = Mathf.Clamp(rb.velocity.x, -max_speed, max_speed);
        newVelocity.y = Mathf.Clamp(rb.velocity.y, -max_speed, max_speed);

        rb.velocity = newVelocity;
    }

    public void ThrustForward(float ammount)
    {
        Vector2 force = transform.up * ammount * speed;

        rb.AddForce(force);
        ClampVelocity();
        current_speed = rb.velocity.magnitude;
    }

    public void OnCollisionEnter2D(Collision2D col)
    {
        StartExplosion();
    }

    public void StartExplosion()
    {
        rb.constraints = RigidbodyConstraints2D.FreezeAll;

        ParticleSystem particles = GetComponent<ParticleSystem>();
        if (particles != null)
            particles.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);

        TrailRenderer trail = GetComponent<TrailRenderer>();
        if (trail != null)
            trail.Clear();

        foreach (GameObject ex in extras)
        {
            ParticleSystem p = ex.GetComponent<ParticleSystem>();
            if (p != null)
                p.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);

            TrailRenderer t = ex.GetComponent<TrailRenderer>();
            if (t != null)
                t.Clear();
        }

        Collider2D myCol = GetComponent<Collider2D>();
        myCol.enabled = false;

        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, explosion_radius);
        foreach (Collider2D objCol in colliders)
        {
            Rigidbody2D rb = objCol.GetComponent<Rigidbody2D>();

            if (rb != null)
            {
                Vector3 dir = objCol.transform.position - transform.position;
                dir.Normalize();
                rb.AddForce(explosion_force * dir);
            }

            ShipInterface i = objCol.GetComponent<ShipInterface>();
            if (i != null)
            {
                i.TakeDamage(damage);
            }
        }
        foreach (GameObject ex in extras)
        {
            Animator anim = ex.GetComponent<Animator>();
            if (anim != null)
                anim.SetTrigger("OnContact");
        }
        Animator a = gameObject.GetComponent<Animator>();
        if (a != null)
        {
            a.SetTrigger("OnContact");
            StartCoroutine(DestroyAfterAnimation());
        }
        else
            DestroyProjectile();
    }

    public void DestroyProjectile()
    {
        //AudioManager.PlayExplosion();
        Destroy(gameObject.transform.root.gameObject);
    }

    public IEnumerator LifeTime()
    {
        yield return lifeTime;

        StartExplosion();
    }

    public IEnumerator DestroyAfterAnimation()
    {
        Animator a = gameObject.GetComponent<Animator>();
        while (a.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            // Aparently at the start we are still not in the explosion animation
            yield return null;
        }

        while (a.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.99f )
        {
            yield return null;
        }
        DestroyProjectile();
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosion_radius);
    }
}
