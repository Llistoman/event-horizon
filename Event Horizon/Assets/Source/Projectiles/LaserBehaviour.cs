﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBehaviour : MonoBehaviour
{
    public float damage_per_tick;
    public float cooldown_time = 0.25f;

    private WaitForSeconds cdw;
    private bool canDamage;

    // Start is called before the first frame update
    void Start()
    {
        canDamage = true;
        cdw = new WaitForSeconds(cooldown_time);
    }

    private void OnEnable()
    {
        canDamage = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (canDamage)
        {
            ShipInterface i = collision.GetComponent<ShipInterface>();
            if (i != null)
            {
                i.TakeDamage(damage_per_tick);
                StartCoroutine(Cooldown());
            }
        }
    }

    IEnumerator Cooldown()
    {
        canDamage = false;
        yield return cdw;
        canDamage = true;
    }

}
