﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelArea : MonoBehaviour
{

    public float tiling = 3.0f;

    public float minX;
    public float maxX;
    public float minY;
    public float maxY;
    public Bounds bounds;

    private List<Collider2D> all_objects;
    private Vector3 scale;
    private float cooldown = 0.2f;
    private WaitForSeconds cdw;
    private bool canTeleport = true;

    // Start is called before the first frame update
    private void Start()
    {
        scale = transform.localScale / tiling / 2.0f;
        minX = -scale.x;
        maxX = scale.x;
        minY = -scale.y;
        maxY = scale.y;
        bounds = new Bounds(transform.position, scale * 2.0f);
        all_objects = new List<Collider2D>();
        cdw = new WaitForSeconds(cooldown);
        canTeleport = true;
    }

    // Update is called once per frame
    private void Update()
    {

    }

    private void FixedUpdate()
    {
        DetectBounds();
        foreach (Collider2D obj in all_objects)
        {
            Vector3 pos = obj.transform.position;
            Rigidbody2D rb = obj.transform.gameObject.GetComponent<Rigidbody2D>();
            if (rb != null && canTeleport)
            {
                Vector3 speed = new Vector3(rb.velocity.x, rb.velocity.y, 0.0f);
                bool tp = false;
                if (pos.x < minX)
                {
                    pos.x += 66.0f;
                    obj.transform.position = pos + speed * Time.deltaTime;
                    tp = true;
                }
                else if (pos.x > maxX)
                {
                    pos.x -= 66.0f;
                    obj.transform.position = pos + speed * Time.deltaTime;
                    tp = true;
                }

                if (pos.y < minY)
                {
                    pos.y += 66.0f;
                    obj.transform.position = pos + speed * Time.deltaTime;
                    tp = true;
                }
                else if (pos.y > maxY)
                {
                    pos.y -= 66.0f;
                    obj.transform.position = pos + speed * Time.deltaTime;
                    tp = true;
                }

                if (tp && canTeleport)
                    StartCoroutine(CooldownFunc());
            }
        }
    }

    private void DetectBounds()
    {
        // Generate list of everything that is in bounds
        all_objects.Clear();

        Collider2D[] objects = Physics2D.OverlapBoxAll(transform.position, transform.localScale, 0.0f);

        all_objects.AddRange(objects);
    }

    private IEnumerator CooldownFunc()
    {
        canTeleport = false;
        yield return cdw;
        canTeleport = true;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(transform.position, scale * 2);
    }
}
