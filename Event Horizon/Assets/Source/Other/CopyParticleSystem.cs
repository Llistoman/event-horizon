﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyParticleSystem : MonoBehaviour
{
    public GameObject from;

    private ParticleSystem otherSystem;
    private ParticleSystem.Particle[] data;
    private ParticleSystem mySystem;

    // Start is called before the first frame update
    void Start()
    {
        otherSystem = from.GetComponent<ParticleSystem>();
        mySystem = GetComponent<ParticleSystem>();
        data = new ParticleSystem.Particle[otherSystem.main.maxParticles];
    }

    // Update is called once per frame
    void LateUpdate()
    {
        int res = otherSystem.GetParticles(data);
        mySystem.SetParticles(data, res);
    }
}
