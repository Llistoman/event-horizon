﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHole : MonoBehaviour
{
    // Private
    private List<Collider2D> affected_objects;
    private bool currentPlayerIsIn = false;
    private bool previousPlayerIsIn = false;

    [Header("Black Hole Parameters")]
    public float gravity_radius;
    public float event_horizon_radius;
    public float gravity_strength;
    public float min_time_scale = 0.5f;

    // Start is called before the first frame update
    private void Start()
    {
        affected_objects = new List<Collider2D>();
    }

    // Update is called once per frame
    private void Update()
    {
        
    }

    private void FixedUpdate()
    {
        // Apply force to all affected objects, if they are in the event horizon, kill them
        // If the player is inside the radius of gravity, maybe distort timescale for cool effects
        DetectAffectedObjects();

        currentPlayerIsIn = false;
        foreach (Collider2D obj in affected_objects)
        {
            float dist2obj = Vector3.Distance(obj.transform.position, transform.position);
            if (dist2obj <= gravity_radius)
            {
                Vector3 gravityDir = transform.position - obj.transform.position;
                gravityDir.Normalize();

                Rigidbody2D objRb = obj.GetComponent<Rigidbody2D>();
                if (objRb != null)
                {
                    float diffAngle = Vector3.SignedAngle(gravityDir, obj.transform.up, -transform.forward);
                    diffAngle = Mathf.Clamp(diffAngle, -1.0f, 1.0f);
                    objRb.AddTorque(diffAngle * 5);
                    objRb.AddForce(gravityDir * gravity_strength);
                }

                PlayerInput player = obj.GetComponent<PlayerInput>();
                if (player != null)
                {
                    currentPlayerIsIn = true;
                    float objTimeDistance = dist2obj - event_horizon_radius;
                    float timeDistance = gravity_radius - event_horizon_radius;
                    float timescale = objTimeDistance / timeDistance;

                    //map(value, low1, high1, low2, high2)
                    //0.5 + (value - 0) * (1 - 0.5) / (1 - 0)
                    timescale = 0.5f + timescale * (1.0f - 0.5f) / (1);
                    Time.timeScale = timescale;
                }
            }
            if (dist2obj <= event_horizon_radius)
            {
                ShipInterface s = obj.GetComponentInChildren<ShipInterface>();
                if (s != null)
                {
                    s.TakeDamage(s.hp);
                }
                else
                {
                    Destroy(obj.gameObject.transform.root.gameObject);
                }
            }
        }

        if (previousPlayerIsIn == true && currentPlayerIsIn == false)
        {
            Time.timeScale = 1.0f;
        }
        previousPlayerIsIn = currentPlayerIsIn;
    }

    // Detect all affected objects
    private void DetectAffectedObjects()
    {
        // Generate list of everything that can be affected by gravity
        affected_objects.Clear();

        Collider2D[] objects = Physics2D.OverlapCircleAll(transform.position, gravity_radius);

        affected_objects.AddRange(objects);
    }

    // Debug Info
    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, gravity_radius);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, event_horizon_radius);
    }
}
