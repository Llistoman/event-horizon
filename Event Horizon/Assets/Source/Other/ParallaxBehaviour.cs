﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBehaviour : MonoBehaviour
{
    // Private
    private MeshRenderer mr;

    [Header("Parallax Parameters")]
    public float parallax_ammount = 4.0f;
    public float tiling = 3.0f;
    public bool auto = false;

    private GameObject player;
    private Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        mr = GetComponent<MeshRenderer>();
        if (!auto)
        {
            player = GameObject.Find("PlayerShip");
            rb = player.GetComponent<Rigidbody2D>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 offset = mr.material.GetTextureOffset("_BaseMap");

        if (!auto)
        {
            offset.x += rb.velocity.x / transform.lossyScale.x / parallax_ammount * Time.deltaTime;
            offset.y += rb.velocity.y / transform.lossyScale.y / parallax_ammount * Time.deltaTime;
        }
        else
        {
            offset.x += Time.deltaTime / transform.lossyScale.x / parallax_ammount;
            offset.y += Time.deltaTime / transform.lossyScale.y / parallax_ammount;
        }
        mr.material.SetTextureOffset("_BaseMap", offset);
    }
}
