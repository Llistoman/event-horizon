﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowGameObject : MonoBehaviour
{
    // References
    public GameObject obj;
    public Vector3 offset;
    public bool rotateWithObj = false;

    // Start is called before the first frame update
    private void Start()
    {
        //player = GameObject.Find("Player");
    }

    // Update is called once per frame
    private void Update()
    {
        Vector3 objPos = obj.transform.position;
        Vector3 newPos = objPos + offset;
        newPos.z = transform.position.z;
        gameObject.transform.position = newPos;

        if (rotateWithObj)
            gameObject.transform.rotation = obj.transform.rotation;
    }
}
