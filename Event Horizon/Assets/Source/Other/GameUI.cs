﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameUI : MonoBehaviour
{
    public static float score = 0.0f;

    public GameObject score_display;
    public GameObject bomb_display1;
    public GameObject bomb_display2;
    public GameObject bomb_display3;

    private GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("PlayerShip");
    }

    // Update is called once per frame
    void Update()
    {
        score_display.GetComponent<TextMeshProUGUI>().text = ((int)score).ToString();

        int bombs = player.GetComponent<PlayerInput>().current_black_holes;
        switch (bombs)
        {
            case 3:
                bomb_display1.SetActive(true);
                bomb_display2.SetActive(true);
                bomb_display3.SetActive(true);
                break;
            case 2:
                bomb_display1.SetActive(true);
                bomb_display2.SetActive(true);
                bomb_display3.SetActive(false);
                break;
            case 1:
                bomb_display1.SetActive(true);
                bomb_display2.SetActive(false);
                bomb_display3.SetActive(false);
                break;
            default:
                bomb_display1.SetActive(false);
                bomb_display2.SetActive(false);
                bomb_display3.SetActive(false);
                break;
        }
    }
}
