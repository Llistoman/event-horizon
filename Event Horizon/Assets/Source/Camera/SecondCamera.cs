﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SecondCamera : MonoBehaviour
{
    // References
    public RenderTexture rt;
    public GameObject rawImage;

    private Camera cam;
    private RawImage img;
    private float aspectRatio;
    private float sizeX;
    private float sizeY;

    // Start is called before the first frame update
    private void Start()
    {
        rt = new RenderTexture(Screen.width, Screen.height, 0, RenderTextureFormat.ARGB32);
        rt.isPowerOfTwo = false;
        rt.filterMode = FilterMode.Point;
        cam = GetComponent<Camera>();
        cam.targetTexture = rt;

        img = rawImage.GetComponent<RawImage>();
        img.color = Color.white;
        img.texture = rt;
    }

    // Update is called once per frame
    private void Update()
    {

    }
}
