﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetRenderTexture : MonoBehaviour
{
    public GameObject cam;
    public RenderTexture rt;

    private RawImage img; 
    // Start is called before the first frame update
    void Start()
    {
        img = GetComponent<RawImage>();
        Camera c = cam.GetComponent<Camera>();
        rt = c.activeTexture;

        img.texture = rt;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
