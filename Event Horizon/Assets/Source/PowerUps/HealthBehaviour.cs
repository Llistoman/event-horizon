﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBehaviour : MonoBehaviour
{
    public float healthRecover = 20;
    public float score;

    // Start is called before the first frame update
    private void Start()
    {
        
    }

    // Update is called once per frame
    private void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        ShipInterface i = collision.gameObject.GetComponent<ShipInterface>();
        if (i != null)
        {
            i.TakeDamage(-healthRecover);
            if (collision.gameObject.tag == "Player")
                GameUI.score += score;
            Destroy(transform.root.gameObject);
        }
    }
}
