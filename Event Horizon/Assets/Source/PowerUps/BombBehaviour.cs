﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombBehaviour : MonoBehaviour
{
    public List<GameObject> extras;
    public float activation_time = 3.0f;
    public float score;
    public GameObject black_hole_prefab;
    public bool activated = false;
    public bool firstTime = true;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (activated && firstTime)
        {
            StartCoroutine(ActivateBlackHole());
            firstTime = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!activated && collision.gameObject.tag == "Player")
        {
            PlayerInput player = collision.gameObject.GetComponent<PlayerInput>();
            player.AddBombs(1);
            GameUI.score += score;
            Destroy(gameObject.transform.root.gameObject);
        }
        else if (!activated && collision.gameObject.tag == "Enemies")
        {
            Destroy(gameObject.transform.root.gameObject);
        }
        else if (!activated && collision.gameObject.tag == "Powerups")
        {
            // do nothing
        }
        else if (!activated && collision.gameObject.tag == "Projectiles")
            activated = true;
    }

    IEnumerator ActivateBlackHole()
    {
        foreach(GameObject ex in extras)
        {
            Animator anim = ex.GetComponent<Animator>();
            if (anim != null)
                anim.SetTrigger("Activated");
        }
        Animator a = GetComponent<Animator>();
        a.SetTrigger("Activated");

        float t = activation_time;
        while (t >= 0.0f)
        {
            foreach (GameObject ex in extras)
            {
                Animator anim = ex.GetComponent<Animator>();
                if (anim != null)
                    anim.speed += 4 * Time.deltaTime;
            }
            a.speed += 4 * Time.deltaTime;

            t -= Time.deltaTime;
            yield return null;
        }

        Instantiate(black_hole_prefab, transform.position, Quaternion.identity);
        
        //Wrapped world stuff
        Instantiate(black_hole_prefab, transform.position - new Vector3(66.0f, 0.0f, 0.0f), Quaternion.identity);
        Instantiate(black_hole_prefab, transform.position + new Vector3(66.0f, 0.0f, 0.0f), Quaternion.identity);
        Instantiate(black_hole_prefab, transform.position - new Vector3(0.0f, 66.0f, 0.0f), Quaternion.identity);
        Instantiate(black_hole_prefab, transform.position + new Vector3(0.0f, 66.0f, 0.0f), Quaternion.identity);

        Destroy(gameObject.transform.root.gameObject);
    }
}
